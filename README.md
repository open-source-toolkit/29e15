# 20Hz至20KHz 0dB测试音频文件合集

欢迎使用本仓库提供的全面频率范围测试音频文件包。本资源专为音频工程师、耳机和扬声器评测者以及音乐爱好者设计，旨在帮助精确校准和测试音频设备的响应能力。此包包含了一系列精心制作的正弦波音频文件，覆盖从深沉的20Hz低频到高频极限的20KHz，每段音频均为30秒，且音量设定在0dB。

### 文件目录结构

- **00. mute.wav**: 静音文件，用于基线测试。
- **01. 20Hz-0dB-30s.wav** 至 **40. 20KHz-0dB-30s.wav**: 分别代表从20Hz到20KHz，每1kHz或间隔递增的单频率正弦波，时长30秒，音量标准为0dB。
- **Infinity zero.wav**: 特殊用途测试文件。
- **LR Channel.wav**: 左右声道测试文件，验证立体声音频播放。
- **musiccut_test.wav**: 示例音乐剪辑，适用于特定测试场景。
- **pink noise 0db.wav**: 0dB粉红噪声，广泛用于均衡器调整和系统测试。
- **Slow_freq_sweep_61_-0dB_44k.16.wav** 和 **Slow_freq_sweep_61_-20dB_44k.16.wav**: 频率扫掠测试文件，分别在-0dB和-20dB进行。
- **Subwoofer-delay-test.wav**: 用于测试低音炮延迟的特别设计文件。
- **左右声道及喇叭极性Sound Check_Channel & Phase_10sec.wav**: 快速检查声道分离和喇叭极性的音频。

### 使用说明

1. **音频设备校准**: 使用这些文件可对您的扬声器或耳机进行频率响应校准，确保每个频率都能准确播放。
2. **测试环境**：在安静环境下使用以获取最准确的测试结果。
3. **专业软件**：建议使用音频编辑或分析工具如Audacity等，来分析播放效果，特别是对于频率响应的精细调整。
4. **安全警告**：高频率可能对听力敏感者造成不适，测试时请注意音量控制，避免听力损伤。

通过本资源包，您将能够细致入微地了解您的音频设备性能，无论是家用音响还是专业录音室设备，都能找到其价值所在。请按照需要选择合适的文件进行测试，并享受探索音频世界的精准之旅。